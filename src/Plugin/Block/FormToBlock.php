<?php

namespace Drupal\form_to_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * form_to_block block.
 *
 * @Block(
 *   id = "form_to_block",
 *   admin_label = @Translation("Form To Block")
 * )
 */
class FormToBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['form_to_block'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select Form'),
    ];

    // getAll form list.
    $results = $this->connection->query("SELECT name, path FROM {router}")->fetchAll();
    $namespace = [];
    foreach ($results as $id => $result) {
      $routeName = $result->name;
      $route = \Drupal::service('router.route_provider')->getRouteByName($routeName);
      $form_title = $route->getDefault('_title');
      $form_namespace = $route->getDefault('_form');
      if($form_title && $form_namespace) {
        $namespace[$form_namespace] = $form_title;
      }
    }
    asort($namespace);
    $form['form_to_block']['form_namespace'] = [
      '#type' => 'select',
      '#title' => $this->t('Form List'),
      '#options' => $namespace,
      '#default_value' => isset($config['form_namespace']) ? $config['form_namespace'] : '',
      '#empty_option' => '-Select-',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('form_namespace', $form_state->getValue(['form_to_block', 'form_namespace']));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $form = \Drupal::formBuilder()->getForm($config['form_namespace']);
    return $form;
  }

}
